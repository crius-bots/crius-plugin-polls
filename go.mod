module gitlab.com/RPGPN/crius-plugin-polls

go 1.16

require (
	github.com/gempir/go-twitch-irc/v2 v2.5.0
	github.com/jmoiron/sqlx v1.3.1
	github.com/shettyh/threadpool v0.0.0-20200323115144-b99fd8aaa945
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/RPGPN/crius-utils v1.5.1
	gitlab.com/RPGPN/criuscommander/v2 v2.4.0
	gitlab.com/ponkey364/golesh-chat v1.1.2
)
