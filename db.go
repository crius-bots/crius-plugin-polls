package crius_plugin_polls

import (
	"github.com/jmoiron/sqlx"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	"time"
)

const tableCreateScript = `CREATE TABLE IF NOT EXISTS polls__polls
(
    id       serial primary key,
    realm_id text not null,
    platform text not null,
    question text not null,
    opt_1    text not null,
    opt_2    text not null,
    opt_3 text,
    opt_4 text,
    ends_at int not null
);

CREATE TABLE IF NOT EXISTS polls__votes
(
    poll_id int references polls__polls on delete cascade,
    realm_id text not null,
    platform text not null,
    user_id text not null,
    vote int not null,

    constraint polls__votes_pk primary key (poll_id, realm_id, platform, user_id)
);`

type poll struct {
	Question string
	Opt1     string  `db:"opt_1"`
	Opt2     string  `db:"opt_2"`
	Opt3     *string `db:"opt_3"`
	Opt4     *string `db:"opt_4"`
	EndsAt   int64   `db:"ends_at"`
}

func (p *poll) IsRunning() bool {
	if p.EndsAt < time.Now().Unix() {
		return false
	}

	return true
}

func getPollID(realmID string, platform cc.PlatformType, db *sqlx.DB) (*int, error) {
	var pollID int
	err := db.Get(&pollID, "SELECT id FROM polls__polls WHERE realm_id=$1 AND platform=$2", realmID, platform.ToString())
	if err != nil {
		return nil, err
	}

	return &pollID, nil
}

func doPollCount(pollID int, db *sqlx.DB) (*pollCount, error) {
	count := &pollCount{}

	// i hate everything about this but it works?!
	err := db.Get(count, `SELECT 
       ( SELECT COUNT(*) FROM polls__votes WHERE poll_id = $1 AND vote = 1 ) AS Opt1,
       ( SELECT COUNT(*) FROM polls__votes WHERE poll_id = $1 AND vote = 2 ) AS Opt2,
       ( SELECT COUNT(*) FROM polls__votes WHERE poll_id = $1 AND vote = 3 ) AS Opt3,
       ( SELECT COUNT(*) FROM polls__votes WHERE poll_id = $1 AND vote = 4 ) AS Opt4,
       ( SELECT COUNT(*) FROM polls__votes WHERE poll_id=$1 ) AS TotalVotes`, pollID)
	if err != nil {
		return nil, err
	}

	return count, nil
}
