package crius_plugin_polls

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/gempir/go-twitch-irc/v2"
	"github.com/jmoiron/sqlx"
	"github.com/shettyh/threadpool"
	CriusUtils "gitlab.com/RPGPN/crius-utils"
	"gitlab.com/RPGPN/crius-utils/GuildCrius"
	"gitlab.com/RPGPN/crius-utils/LiveCrius"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	goleshchat "gitlab.com/ponkey364/golesh-chat"
	"strconv"
	"time"
)

type pollCount struct {
	Opt1       uint
	Opt2       uint
	Opt3       *uint
	Opt4       *uint
	TotalVotes uint
}

var PluginInfo = &cc.PluginJSON{
	Name:               "Polls",
	License:            "BSD-3-Clause",
	Creator:            "Ben <Ponkey364>",
	URL:                "https://gitlab.com/RPGPN/crius-plugin-polls/",
	Description:        "Run polls in chat",
	SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
	Commands: []*cc.PJCommand{
		{
			Name:               "Start Poll",
			HandlerName:        "startpoll",
			Activator:          "startpoll",
			Help:               "Start a poll (only one can be running at a time). Usage: `startpoll [question] [time (e.g. 5m / 10m, max 15m) [opt 1] [opt 2] [optional: opt 3] [optional: opt 4]`",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
		},
		{
			Name:               "Vote in Poll",
			HandlerName:        "pollvote",
			Activator:          "pollvote",
			Help:               "Vote in the currently running poll. Usage: `pollvote [option number]",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
		},
		{
			Name:               "End Poll",
			HandlerName:        "endpoll",
			Activator:          "endpoll",
			Help:               "End the currently running poll and see the results.",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
		},
		{
			Name:               "Poll Options",
			HandlerName:        "pollopts",
			Activator:          "pollopts",
			Help:               "See the options for the currently running poll",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
		},
	},
}

type polls struct {
	db   *sqlx.DB
	pool *threadpool.ScheduledThreadPool
}

func Setup(ctx context.Context) (map[string]cc.CommandHandler, error) {
	// get stuff we need from ctx
	s := CriusUtils.GetSettings(ctx)

	db := s.GetDB()

	// run the table creation script from db.go
	// create a transaction
	tx := db.MustBegin()

	// run the script
	_, err := tx.Exec(tableCreateScript)
	if err != nil {
		return nil, err
	}

	// done
	err = tx.Commit()
	if err != nil {
		return nil, err
	}

	pool := threadpool.NewScheduledThreadPool(4)

	p := &polls{
		db:   db,
		pool: pool,
	}

	return map[string]cc.CommandHandler{
		"startpoll": p.StartPoll,
		"pollvote":  p.PollVote,
		"endpoll":   p.EndPoll,
		"pollopts":  p.PollOpts,
	}, nil
}

func (p *polls) StartPoll(m *cc.MessageContext, args []string) error {
	if len(args) < 4 {
		m.Send("Usage: `startpoll [question] [time (e.g. 5m / 10m, max 15m) [opt 1] [opt 2] [optional: opt 3] [optional: opt 4]`")
		return nil
	}

	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID
	switch platform {
	case cc.PlatformDiscord:
		{
			isAdmin, err := GuildCrius.GetPermissions(m.Context).IsServerAdmin(m.Author.ID, m.RealmID)
			if err != nil {
				return err
			}

			if !isAdmin {
				m.Send("Only server admins can start polls.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)

			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("Only the streamer can start polls.")
				return nil
			}
		}
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitch.PrivateMessage)
			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("Only the streamer or moderators can start polls.")
				return nil
			}
		}
	}

	// is there already a running poll?
	running, err := isPollRunning(realmID, platform, p.db)
	if err != nil {
		return err
	}

	if running {
		m.Send("you can only run one poll at a time.")
		return nil
	}

	var (
		opt3 *string
		opt4 *string
	)
	if len(args) == 6 {
		opt3 = &args[4]
		opt4 = &args[5]
	} else if len(args) == 5 {
		opt3 = &args[4]
		opt4 = nil
	} else if len(args) < 5 {
		opt3 = nil
		opt4 = nil
	}

	dur, err := time.ParseDuration(args[1])
	if err != nil {
		return err
	}

	if dur > time.Minute*15 {
		// we're not going to allow anything over 15m, sorry folks
		m.Send("The poll timer has a maximum of 15 minutes")
		return nil
	}

	endsAt := time.Now().Add(dur)

	_, err = p.db.Exec(`INSERT INTO polls__polls (realm_id, platform, question, opt_1, opt_2, opt_3, opt_4, ends_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`, realmID, platform.ToString(), args[0], args[2], args[3], opt3, opt4,
		endsAt.Unix())
	if err != nil {
		return err
	}

	m.Send("Created a poll. Here are the options:")
	m.Send("1: " + args[2])
	m.Send("2: " + args[3])
	if opt3 != nil {
		m.Send("3: " + *opt3)
	}
	if opt4 != nil {
		m.Send("4: " + *opt4)
	}

	task := endPollTask{
		m:        m,
		db:       p.db,
		realmID:  realmID,
		platform: platform,
	}
	p.pool.ScheduleOnce(task, dur)

	return nil
}

func (p *polls) PollVote(m *cc.MessageContext, args []string) error {
	if len(args) == 0 {
		m.Send("You must provide an option to vote for.")
		return nil
	}

	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID
	userID := m.Author.ID

	// is there a running poll?
	running, err := isPollRunning(realmID, platform, p.db)
	if err != nil {
		return err
	}

	if !running {
		m.Send("there are no polls running in this realm.")
		return nil
	}

	// what's the poll id
	pollID, err := getPollID(realmID, platform, p.db)
	if err != nil {
		return err
	}

	thisPoll := &poll{}
	err = p.db.Get(thisPoll, `SELECT question, opt_1, opt_2, opt_3, opt_4, ends_at FROM polls__polls WHERE id=$1`,
		*pollID)
	if err != nil {
		return err
	}

	// is this poll running ?
	if !thisPoll.IsRunning() {
		// we're going to do nothing since the fn to clean up the db obvs hasn't happened yet.
		return nil
	}

	var dbVote *int
	err = p.db.Get(&dbVote, "SELECT vote FROM polls__votes WHERE poll_id=$1 AND user_id=$2", *pollID, userID)
	if err != nil && err != sql.ErrNoRows {
		return err
	}
	if dbVote != nil {
		m.Send("You have already voted.")
		return nil
	}

	// they haven't already voted
	vote, err := strconv.Atoi(args[0])
	if err != nil {
		return err
	}

	if thisPoll.Opt3 == nil && vote >= 3 {
		m.Send("There are only two options.")
		return nil
	} else if thisPoll.Opt4 == nil && vote == 4 {
		m.Send("There are only three options.")
		return nil
	}

	_, err = p.db.Exec(`INSERT INTO polls__votes (poll_id, realm_id, platform, user_id, vote)
		VALUES ($1, $2, $3, $4, $5)`, *pollID, realmID, platform.ToString(), userID, vote)
	if err != nil {
		return err
	}

	return nil
}

func (p *polls) EndPoll(m *cc.MessageContext, _ []string) error {
	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID
	switch platform {
	case cc.PlatformDiscord:
		{
			isAdmin, err := GuildCrius.GetPermissions(m.Context).IsServerAdmin(m.Author.ID, m.RealmID)
			if err != nil {
				return err
			}

			if !isAdmin {
				m.Send("Only server admins can add new quotes.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)

			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("Only the streamer can add new quotes.")
				return nil
			}
		}
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitch.PrivateMessage)
			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("Only the streamer or moderators can add new quotes.")
				return nil
			}
		}
	}

	err := finishPoll(realmID, platform, m, p.db)
	if err != nil {
		return err
	}

	return nil
}

func (p *polls) PollOpts(m *cc.MessageContext, _ []string) error {
	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID

	// is there a running poll?
	running, err := isPollRunning(realmID, platform, p.db)
	if err != nil {
		return err
	}

	if !running {
		m.Send("there are no polls running in this realm.")
		return nil
	}

	pollID, err := getPollID(realmID, platform, p.db)
	if err != nil {
		return err
	}

	thisPoll := &poll{}
	err = p.db.Get(thisPoll, "SELECT question, opt_1, opt_2, opt_3, opt_4, ends_at FROM polls__polls WHERE id=$1",
		*pollID)
	if err != nil {
		return err
	}

	m.Send("The currently running poll has the follwing options:")
	m.Send("1: " + thisPoll.Opt1)
	m.Send("2: " + thisPoll.Opt2)
	if thisPoll.Opt3 != nil {
		m.Send("3: " + *thisPoll.Opt3)
	}
	if thisPoll.Opt4 != nil {
		m.Send("4: " + *thisPoll.Opt4)
	}

	return nil
}

func isPollRunning(realmID string, platform cc.PlatformType, db *sqlx.DB) (bool, error) {
	var question string
	err := db.Get(&question, "SELECT question from polls__polls WHERE realm_id=$1 AND platform=$2", realmID,
		platform.ToString())
	if err != nil && err != sql.ErrNoRows {
		return false, err
	}

	if errors.Is(err, sql.ErrNoRows) {
		return false, nil
	}

	return true, nil
}

func finishPoll(realmID string, platform cc.PlatformType, m *cc.MessageContext, db *sqlx.DB) error {
	// is there a running poll?
	running, err := isPollRunning(realmID, platform, db)
	if err != nil {
		return err
	}

	if !running {
		m.Send("there are no polls running in this realm.")
		return nil
	}

	// what is the running poll's ID?
	pollID, err := getPollID(realmID, platform, db)
	if err != nil {
		return err
	}

	stats, err := doPollCount(*pollID, db)
	if err != nil {
		return err
	}

	thisPoll := &poll{}
	err = db.Get(thisPoll, "SELECT question, opt_1, opt_2, opt_3, opt_4, ends_at FROM polls__polls WHERE id=$1",
		*pollID)
	if err != nil {
		return err
	}

	m.Send("The results are in:")
	m.Send(fmt.Sprintf("%s: %.1f%%", thisPoll.Opt1, float64(stats.Opt1)/float64(stats.TotalVotes)*100))
	m.Send(fmt.Sprintf("%s: %.1f%%", thisPoll.Opt2, float64(stats.Opt2)/float64(stats.TotalVotes)*100))
	if thisPoll.Opt3 != nil {
		m.Send(fmt.Sprintf("%s: %.1f%%", *thisPoll.Opt3, float64(*stats.Opt3)/float64(stats.TotalVotes)*100))
	}
	if thisPoll.Opt4 != nil {
		m.Send(fmt.Sprintf("%s: %.1f%%", *thisPoll.Opt4, float64(*stats.Opt4)/float64(stats.TotalVotes)*100))
	}

	// now clear the db. there's a cascade, so we only need to yoink the polls__polls table entry
	_, err = db.Exec("DELETE FROM polls__polls WHERE id=$1", *pollID)
	if err != nil {
		return err
	}

	return nil
}
